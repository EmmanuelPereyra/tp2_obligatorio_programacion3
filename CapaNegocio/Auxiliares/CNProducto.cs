﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio.Auxiliares
{
    public class CNProducto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public float Precio { get; set; }
        public string Url_Imagen { get; set; }
        public bool Es_Oferta { get; set; }
        public bool Es_Destacado { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio.Auxiliares
{
    public class CNCategoria
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}

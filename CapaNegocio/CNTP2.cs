﻿using CapaDatos.Metodos;
using CapaNegocio.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CNTP2
    {
        public static List<CNProducto> ObtenerProductos()
        {
            try
            {
                var capa_datos = EF.ObtenerProductos();

                return capa_datos.Select(x => new CNProducto
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Precio = x.Precio,
                    Url_Imagen = x.Url_Imagen,
                    Es_Oferta = x.Es_Oferta,
                    Es_Destacado = x.Es_Destacado
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<CNCategoria> ObtenerCategorias()
        {
            try
            {
                var capa_datos = EF.ObtenerCategorias();

                return capa_datos.Select(x => new CNCategoria
                {
                    Id = x.Id,
                    Nombre = x.Nombre
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<CNSubcategoria> ObtenerSubcategorias()
        {
            try
            {
                var capa_datos = EF.ObtenerSubcategorias();

                return capa_datos.Select(x => new CNSubcategoria
                {
                    Id = x.Id,
                    Id_Categoria = x.Id_Categoria,
                    Nombre = x.Nombre
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AgregarConsulta(string nombre, string apellido, string correo, string asunto, string mensaje)
        {
            try
            {
                bool correcto = false;

                if (ValidarDatos(nombre, apellido, correo, asunto, mensaje))
                {
                    var capa_datos = EF.AgregarConsulta(nombre, correo, asunto, mensaje);

                    if (capa_datos)
                    {
                        correcto = true;
                    }
                }

                return correcto;
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public static bool ValidarDatos(string nombre, string apellido, string correo, string asunto, string mensaje)
        {
            bool correcto = true;
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            try
            {
                if (nombre.Length < 4 || nombre.Length > 30 || apellido.Length < 4 || apellido.Length > 30)
                {
                    correcto = false;
                }
                else if (!Regex.IsMatch(nombre, @"^[a-zA-Z]+$"))
                {
                    correcto = false;
                }
                else if (!Regex.IsMatch(correo,expresion))
                {
                    correcto = false;
                }
                else if (asunto.Length > 20)
                {
                    correcto = false;
                }
                else if (mensaje.Length < 10 || mensaje.Length > 100)
                {
                    correcto = false;
                }

                return correcto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

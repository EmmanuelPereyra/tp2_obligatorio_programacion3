﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class TiendaProductos
    {
        public static List<CNProducto> ObtenerProductos()
        {
            return new List<CNProducto>
            {
                new CNProducto
                {
                    ID = "1",
                    DESCRIPCION = "XDJ-RX2",
                    PRECIO = 1400,
                    FOTO = "Images/destacado1.png"
                },
                new CNProducto
                {
                    ID = "2",
                    DESCRIPCION = "DJS-1000",
                    PRECIO = 1176,
                    FOTO = "Images/ofertas1.png"
                },
                new CNProducto
                {
                    ID = "3",
                    DESCRIPCION = "CDJ-2000NXS2",
                    PRECIO = 2199,
                    FOTO = "Images/destacado2.png"
                },
                new CNProducto
                {
                    ID = "4",
                    DESCRIPCION = "DDJ-RZX",
                    PRECIO = 999,
                    FOTO = "Images/ofertas2.png"
                },
                new CNProducto
                {
                    ID = "5",
                    DESCRIPCION = "DJM-900NXS2",
                    PRECIO = 2199,
                    FOTO = "Images/destacado3.png"
                },
                new CNProducto
                {
                    ID = "6",
                    DESCRIPCION = "HDJ-X5",
                    PRECIO = 99,
                    FOTO = "Images/ofertas3.png"
                },
                new CNProducto
                {
                    ID = "7",
                    DESCRIPCION = "DJ player",
                    PRECIO = 0,
                    FOTO = ""
                },
                new CNProducto
                {
                    ID = "8",
                    DESCRIPCION = "DJ mixer",
                    PRECIO = 0,
                    FOTO = ""
                },
                new CNProducto
                {
                    ID = "9",
                    DESCRIPCION = "DJM-V10",
                    PRECIO = 0,
                    FOTO = ""
                },
                new CNProducto
                {
                    ID = "10",
                    DESCRIPCION = "DJM-TOUR1",
                    PRECIO = 0,
                    FOTO = ""
                },
                new CNProducto
                {
                    ID = "11",
                    DESCRIPCION = "DJM-900NXS2",
                    PRECIO = 0,
                    FOTO = ""
                },
                new CNProducto
                {
                    ID = "12",
                    DESCRIPCION = "DJM-450",
                    PRECIO = 0,
                    FOTO = ""
                },
                new CNProducto
                {
                    ID = "13",
                    DESCRIPCION = "DJ sampler",
                    PRECIO = 0,
                    FOTO = ""
                },
                new CNProducto
                {
                    ID = "14",
                    DESCRIPCION = "DJ controller",
                    PRECIO = 0,
                    FOTO = ""
                },
                new CNProducto
                {
                    ID = "15",
                    DESCRIPCION = "All in one DJ system",
                    PRECIO = 0,
                    FOTO = ""
                },
                new CNProducto
                {
                    ID = "16",
                    DESCRIPCION = "Headphones",
                    PRECIO = 0,
                    FOTO = ""
                }
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CNProducto
    {
        public string ID { get; set; }
        public string DESCRIPCION { get; set; }
        public float PRECIO { get; set; }
        public string FOTO { get; set; }
    }
}

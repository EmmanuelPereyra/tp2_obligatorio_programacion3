﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapaDatos.Tablas
{
    [Table("Productos")]
    public class Producto
    {
        public Producto()
        {

        }

        [Key]
        [Column("Id_Producto", Order = 1)]
        public int Id { get; set; }

        [Required, MaxLength(50)]
        [Column("Nombre", Order = 2)]
        public string Nombre { get; set; }

        [Required]
        [Column("Precio", Order = 3)]
        public float Precio { get; set; }

        [Required, MaxLength(100)]
        [Column("Url_Imagen", Order = 4)]
        public string Url_Imagen { get; set; }

        [Required]
        [Column("Es_Oferta", Order = 5)]
        public bool Es_Oferta { get; set; }

        [Required]
        [Column("Es_Destacado", Order = 6)]
        public bool Es_Destacado { get; set; }
    }
}
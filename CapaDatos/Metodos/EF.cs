﻿using CapaDatos.Tablas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Metodos
{
    public class EF
    {
        public static List<Producto> ObtenerProductos()
        {
            try
            {
                using (var context = new TP2())
                {
                    return context.Productos.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Categoria> ObtenerCategorias()
        {
            try
            {
                using (var context = new TP2())
                {
                    return context.Categorias.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Subcategoria> ObtenerSubcategorias()
        {
            try
            {
                using (var context = new TP2())
                {
                    return context.Subcategorias.Include("FK_Id_Categoria").ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AgregarConsulta(string nombre, string correo, string asunto, string mensaje)
        {
            try
            {
                using (var context = new TP2())
                {
                    var nuevo = new Consulta
                    {
                        Nombre = nombre,
                        Correo = correo,
                        Asunto = asunto,
                        Mensaje = mensaje
                    };

                    context.Consultas.Add(nuevo);

                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP2_Obligatorio.Models
{
    public class Producto
    {
        public string ID { get; set; }
        public string DESCRIPCION { get; set; }
        public float PRECIO { get; set; }
        public string FOTO { get; set; }
    }
}
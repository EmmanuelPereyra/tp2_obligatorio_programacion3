﻿using CapaNegocio.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP2_Obligatorio.Models
{
    public class ProductosModel
    {
        public List<CNProducto> Productos { get; set; }
        public List<CNCategoria> Categorias { get; set; }
        public List<CNSubcategoria> Subcategorias { get; set; }
        
    }
}
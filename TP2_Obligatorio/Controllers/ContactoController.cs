﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.WebPages;

namespace TP2_Obligatorio.Controllers
{
    public class ContactoController : Controller
    {
        // GET: Contacto
        public ActionResult Contacto()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string nombre, string apellido, string correo, string asunto, string mensaje, string alerta)
        {
            var respuesta = CNTP2.AgregarConsulta(nombre, apellido, correo, asunto, mensaje);
            if (respuesta == false)
            {
                alerta = "Por favor, revise los campos e intente nuevamente";
            }

            return Json(alerta);
        }
    }
}
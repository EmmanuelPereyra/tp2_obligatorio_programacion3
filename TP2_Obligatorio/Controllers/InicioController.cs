﻿using CapaNegocio;
using CapaNegocio.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP2_Obligatorio.Models;

namespace TP2_Obligatorio.Controllers
{
    public class InicioController : Controller
    {
        // GET: Inicio
        public ActionResult Inicio()
        {
            var productos = CNTP2.ObtenerProductos();
            var categorias = CNTP2.ObtenerCategorias();
            var subcategorias = CNTP2.ObtenerSubcategorias();

            var modelo = new ProductosModel
            {
                Productos = productos.Select(x => new CNProducto
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Precio = x.Precio,
                    Url_Imagen = x.Url_Imagen,
                    Es_Oferta = x.Es_Oferta,
                    Es_Destacado = x.Es_Destacado
                }).ToList(),
                Categorias = categorias.Select(x => new CNCategoria
                {
                    Id = x.Id,
                    Nombre = x.Nombre
                }).ToList(),
                Subcategorias = subcategorias.Select(x => new CNSubcategoria {
                    Id = x.Id,
                    Id_Categoria = x.Id_Categoria,
                    Nombre = x.Nombre
                }).ToList()
            };

            return View(modelo);
        }
    }
}
$(document).ready(function () {
	var map = new google.maps.Map(document.getElementById("map"), {
		center: { lat: 33.858345, lng: -118.315500 },
		zoom: 18,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	var marker = new google.maps.Marker({
		position: { lat: 33.858345, lng: -118.315500 },
		map: map
	});
});
$(document).ready(function () {
	 $(" .demo").slick();
});

function abrirpopup(idArt) {
	const art = document.getElementById(idArt);
	const popup = document.getElementById("contenedor-popup");
	const padre = document.getElementById("padre");
	 
	document.getElementById("descripcion").innerHTML = "Descripcion: " + art.dataset.descripcion;
	document.getElementById("precio").innerHTML = "Precio: " + art.dataset.precio;
	document.getElementById("imagen").src = art.dataset.imagen;
	padre.classList.add("fondo-black");
	popup.style.opacity = "1";
	popup.style.visibility = "visible";
}

function cerrarPopup() {
	const popup = document.getElementById("contenedor-popup");
	const padre = document.getElementById("padre");
	padre.classList.remove("fondo-black");
	popup.style.opacity = "0";
	popup.style.visibility = "hiden";
}

function crear() {
	 if ($('#nombre').val() != '' && $('#email').val() != '' && $('#asunto').val() != '' && $('#mensaje').val() != '') {
		  $.ajax({
				url: "/Contacto/Create",
				data: {
					 nombre: $('#nombre').val(),
					 apellido: $('#apellido').val(),
					 correo: $('#email').val(),
					 asunto: $('#asunto').val(),
					 mensaje: $('#mensaje').val(),
					 alerta: 'Se envió el formulario correctamente'
				},
				type: "post",
				success: function (data) {
					 if (data != '') {
						  alert(data);
                }
					 window.location.href = "/Contacto/Contacto";
				},
				error: function () {
					 alert("Por favor, revise los campos e intente nuevamente");
				}
		  });
	 }
	 else {
		  alert("Complete todos los campos");
    }
}